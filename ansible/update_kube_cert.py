#!/usr/bin/env python

import yaml, base64, os, uuid, getpass
from pathlib import Path

print("Updating ansible vault with latest kube config")

with open(str(Path.home()) + "/.kube/config", 'r') as stream:
    config = yaml.load(stream)



cluster = config['clusters'][0]['cluster']
user = config['users'][0]['user']

authorityData = base64.b64decode(cluster['certificate-authority-data'])
certificateData = base64.b64decode(user['client-certificate-data'])
keyData = base64.b64decode(user['client-key-data'])

with open('client.crt', 'wb') as file:
    file.write(certificateData)

with open('client.key', 'wb') as file:
    file.write(keyData)

with open('ca.crt', 'wb') as file:
    file.write(authorityData)



password = str(uuid.uuid4())

opensslCmd = "openssl pkcs12 -export -out cert.pfx -inkey client.key -in client.crt -certfile ca.crt -passin pass:{} -passout pass:{}".format(password, password)
os.popen(opensslCmd).read()

base64Cmd = "base64 -w 0 cert.pfx"
certPfxBase64 = os.popen(base64Cmd).read()

vaultPass = getpass.getpass("Enter vault password: ")
vaultPassCmd = "echo {} > .tmpvaultpass".format(vaultPass)
os.popen(vaultPassCmd).read()

decryptCmd = "ansible-vault decrypt --vault-password-file .tmpvaultpass group_vars/vault.yml"
os.popen(decryptCmd).read()

with open('group_vars/vault.yml', 'r') as file:
    vault = yaml.load(file)
    if vault is None:
        vault = {'kubernetes': {
            'cert': { 'password': "",
                    'content': ""
                }
                }
            }

    vault['kubernetes']['cert']['password'] = password
    vault['kubernetes']['cert']['content'] = certPfxBase64

with open('group_vars/vault.yml', 'w') as file:
    yamlStr = yaml.dump(vault, file, default_flow_style=False, allow_unicode=True)

encryptCmd = "ansible-vault encrypt --vault-password-file .tmpvaultpass group_vars/vault.yml"
os.popen(encryptCmd).read()

cleanupCmd = "shred -u .tmpvaultpass"
os.popen(cleanupCmd).read()

print("Done!")

##check
#keytool -list -v -keystore cert.pfx    -storepass PASS    -storetype PKCS12




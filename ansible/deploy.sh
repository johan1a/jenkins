#!/bin/sh
ansible-playbook -K jenkins.yml -i group_vars/hosts.ini --vault-password-file .vaultpass -e jenkins_version=latest

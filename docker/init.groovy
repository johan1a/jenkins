import com.cloudbees.jenkins.plugins.sshcredentials.impl.BasicSSHUserPrivateKey
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.impl.*
import hudson.model.*
import hudson.model.View
import hudson.plugins.groovy.*
import hudson.security.*
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.DslScriptLoader
import javaposse.jobdsl.dsl.JobManagement
import javaposse.jobdsl.plugin.JenkinsJobManagement
import jenkins.model.*
import jenkins.model.*
import net.sf.json.JSONObject
import org.csanchez.jenkins.plugins.kubernetes.KubernetesCloud
import hudson.security.csrf.DefaultCrumbIssuer
import javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration
import jenkins.model.GlobalConfiguration
import org.jenkinsci.plugins.workflow.flow.GlobalDefaultFlowDurabilityLevel
import org.jenkinsci.plugins.workflow.flow.FlowDurabilityHint

println "Configuring maximum security..."
println ""

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin","admin")
def instance = Jenkins.getInstance()
instance.setSecurityRealm(hudsonRealm)
instance.save()

println "Setting up Kubernetes plugin..."
println ""

KubernetesCloud cloud = new KubernetesCloud("kubernetes")
cloud.serverUrl = "k8s0:6443"
cloud.skipTlsVerify = true
cloud.namespace = "default"
cloud.jenkinsUrl = "http://jenkins-master-svc:8080"
cloud.credentialsId = "kubernetes-certificate"
cloud.containerCap = 20
cloud.connectTimeout = 5
Jenkins.getInstance().clouds.add(cloud)

instance.setNumExecutors(0)

println "Creating jobs..."
println ""

JobManagement jobManagement = new JenkinsJobManagement(System.out, [:], new File('.'))
File jobScript = new File('/usr/share/jenkins/seed.groovy')
new DslScriptLoader(jobManagement).with {

  runScript(jobScript.text)

}

println "Use PERFORMANCE_OPTIMIZED durability level"
println ""
instance.getExtensionList(GlobalDefaultFlowDurabilityLevel.DescriptorImpl.class)[0].setDurabilityHint(FlowDurabilityHint.PERFORMANCE_OPTIMIZED)

println "Enable CSRF protection"
if (instance.getCrumbIssuer() == null) {
  instance.setCrumbIssuer(new DefaultCrumbIssuer(true))
}

println "Disable agent to master security subsystem and dismiss the warning"
def rule = instance.getExtensionList(jenkins.security.s2m.MasterKillSwitchConfiguration.class)[0].rule
if (!rule.getMasterKillSwitch()) {
  rule.setMasterKillSwitch(true)
}
instance.getExtensionList(jenkins.security.s2m.MasterKillSwitchWarning.class)[0].disable(true)

println "Configuring views"
println ""

String viewXml(String regex) {

    println "Configuring view $regex"
    """
      <hudson.model.ListView>
        <name>Master</name>
        <filterExecutors>false</filterExecutors>
        <filterQueue>false</filterQueue>
        <properties class='hudson.model.View\$PropertyList'/>
        <jobNames>
          <comparator class='hudson.util.CaseInsensitiveComparator'/>
        </jobNames>
        <jobFilters/>
        <columns>
          <hudson.views.StatusColumn/>
          <hudson.views.WeatherColumn/>
          <hudson.views.JobColumn/>
          <hudson.views.LastSuccessColumn/>
          <hudson.views.LastFailureColumn/>
          <hudson.views.LastDurationColumn/>
          <hudson.views.BuildButtonColumn/>
          <hudson.plugins.favorite.column.FavoriteColumn/>
        </columns>
        <includeRegex>$regex</includeRegex>
        <recurse>true</recurse>
      </hudson.model.ListView>
"""
}
View mainView = View.createViewFromXML("Master", new ByteArrayInputStream(viewXml('.*master*').bytes))
View allView = View.createViewFromXML("All", new ByteArrayInputStream(viewXml('.*').bytes))

instance.views = [allView, mainView]
instance.primaryView = mainView

instance.save()


import javaposse.jobdsl.dsl.DslFactory

DslFactory factory = this


List<String> repos = ['gateway',
                      'jwt-auth',
                      'banking-frontend',
                      'banking-api',
                      'banking-data-collector',
                      'banking',
                      'budget-frontend',
                      'mq',
                      'slack-service',
                      'sbot',
                      'traeskfindr-web',
                      'groovy-langserver',
                      'jenkins',
                      'syncer',
                      'haskell-go',
                      'bc-backend',
                      'bc-frontend',
                      'runtracker-server',
                      'housing-search']

repos.each { project ->

  factory.multibranchPipelineJob("${project}") {
      branchSources {
        git {
            remote("https://gitlab.com/johan1a/${project}.git")
            credentialsId('git-credentials')
            includes('')
        }
    }
    // check every minute for scm changes as well as new / deleted branches
    triggers {
      periodic(1)
    }
  }
}

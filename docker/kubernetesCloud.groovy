import org.csanchez.jenkins.plugins.kubernetes.KubernetesCloud

Jenkins.getInstance().clouds.add(new KubernetesCloud(
        name: "kubernetes",
        serverUrl: "k8s0:6443",
        skipTlsVerify: true,
        namespace: "default",
        jenkinsUrl: "http://jenkins-master-svc:8080",
        credentialsId: "kubernetes-certificate",
        containerCap: 20,
        connectTimeout: 5
))
